# PHP_CodeSniffer sniffs for Falcon coding standards
## Installation
### Install PHP_CodeSniffer and enable rule
 Also adds global Composer bin directory to `PATH`, if necessary:
```bash
composer global require squizlabs/php_codesniffer
ln -s /path/to/repo/Falcon -t ~/.composer/vendor/squizlabs/php_codesniffer/CodeSniffer/Standards/
echo export PATH=\$PATH:~/.composer/vendor/bin >> ~/.bashrc
source ~/.bashrc
```
Check it's worked with `which phpcs`. You can periodically run `composer global update` to update all global packages.
### Enable it in your editor / IDE:
#### PhpStorm
* Go to *Languages & Frameworks > PHP > CodeSniffer* in settings, and choose the path to the phpcs executable (`~/.composer/vendor/bin/phpcs`). Validate it.
* Now go to *Editor > Inspections > PHP > PHP Code Sniffer validation*, tick the box to enable it, refresh the list of available coding standards, and choose "Falcon" from the list.
