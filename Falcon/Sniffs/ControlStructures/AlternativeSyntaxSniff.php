<?php

/**
 * Arrays must be instantiated using [] rather than array() - we are not coding in PHP4
 */
class Falcon_Sniffs_ControlStructures_AlternativeSyntaxSniff implements PHP_CodeSniffer_Sniff
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        return [
            T_ENDDECLARE,
            T_ENDFOR,
            T_ENDFOREACH,
            T_ENDIF,
            T_ENDSWITCH,
            T_ENDWHILE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $phpcsFile->addError('Traditional control structure syntax only.', $stackPtr);
    }
}
