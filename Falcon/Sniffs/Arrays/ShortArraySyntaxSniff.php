<?php

/**
 * Arrays must be instantiated using [] rather than array() - we are not coding in PHP4
 */
class Falcon_Sniffs_Arrays_ShortArraySyntaxSniff implements PHP_CodeSniffer_Sniff
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        return [T_ARRAY];
    }

    /**
     * @inheritdoc
     */
    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $phpcsFile->addError('Short array syntax only.', $stackPtr);
    }
}
