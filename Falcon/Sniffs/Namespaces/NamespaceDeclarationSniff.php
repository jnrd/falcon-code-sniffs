<?php

/**
 * The class namespace declaration must be on the same line as <?php
 */
class Falcon_Sniffs_Namespaces_NamespaceDeclarationSniff implements PHP_CodeSniffer_Sniff
{
    /**
     * @return array
     */
    public function register()
    {
        return [T_OPEN_TAG, T_CONST];
    }

    /**
     * @inheritdoc
     */
    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $namespacePtr = $phpcsFile->findNext(T_NAMESPACE, $stackPtr);

        if ($namespacePtr === false) {
            return;
        }

        $tokens = $phpcsFile->getTokens();

        $openTagToken = $tokens[$stackPtr];
        $namespaceToken = $tokens[$namespacePtr];

        if ($openTagToken['line'] === $namespaceToken['line']) {
            return;
        }

        $phpcsFile->addError('The namespace declaration must be on the same line as the opening tag', $namespacePtr);
    }
}
