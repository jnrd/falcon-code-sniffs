<?php

/**
 * Always use the use keyword - no fully qualified namespaces outside of the use statement
 */
class Falcon_Sniffs_Namespaces_FullyQualifiedNamespaceSniff implements PHP_CodeSniffer_Sniff
{
    private $reportedLines = [];

    /**
     * @return array
     */
    public function register()
    {
        return [T_NS_SEPARATOR];
    }

    /**
     * @inheritdoc
     */
    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $nsSeparatorToken = $tokens[$stackPtr];
        $thisLine = $nsSeparatorToken['line'];

        if (in_array($thisLine, $this->reportedLines)) {
            return;
        }

        foreach ([T_USE, T_NAMESPACE] as $tokenName) {
            $ptr = $phpcsFile->findPrevious($tokenName, $stackPtr);

            if ($ptr !== false) {
                $thisToken = $tokens[$ptr];

                if ($thisLine === $thisToken['line']) {
                    return;
                }
            }
        }

        $this->reportedLines[] = $thisLine;

        $phpcsFile->addError('No fully-qualified namespaces outside of use statements.', $stackPtr);
    }
}
